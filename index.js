const chalk = require('chalk');

const config = require('./config/default.json');
const connectMongoose = require('./src/loaders/mongoose');

const APIServer = require('./src/apps/api');

/**
 * @description bootstrap the server for given configuration
 */
function bootstrap() {
  console.info(chalk.yellow('Bootstrapping server..'));
  connectMongoose(config.database)
    .then(() => {
      console.log('Database connection established!');
      APIServer.start(process.env.PORT || config.port, process.env.HOST || config.host);
    })
    .catch((err) => {
      console.info(chalk.red('Something went wrong while bootstrapping server, we will shutdown gracefully.'));
      console.error(err);
      process.exit(1);
    });
}

bootstrap();
