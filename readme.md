# Getir Case Study

This application must cover all requirements in document which is shared by Getir.
Publicly available for testing in berkaybindebir-case-study.heroku.com

*The source code includes many comments that explain the way of thinking which is generally it shouldn't be in the source code.*

# Install & Run

You can easly run the application using `npm install` then `npm run start` or `npm run listen`.   
You may want to run the application on different port which is defined in **config/default.json** so in this case `PORT=XXXX npm run start`
# Usage
You can make a request to **[POST] berkaybindebir-case-study.herokuapp.com** for fetch records by the given query in the body.

The request body must include **startDate, endDate, minCount and maxCount**

 - startDate must be date and must before endDate
 - endDate must be date and must after startDate
 - minCount must be number and less than maxCount
 - maxCount must be number and bigger than minCount

example curl:

    curl --location --request POST 'http://berkaybindebir-case-study.herokuapp.com/' \
    
    --header 'Content-Type: application/json' \
    
    --data-raw '{
    
    "startDate": "2010-01-01",
    
    "endDate": "2020-01-01",
    
    "minCount": 100,
    
    "maxCount": 220
    
    }'

# Linting

This application uses Eslint for enforce code style and style guide is **eslint airbnb** - https://www.npmjs.com/package/eslint-config-airbnb

You can check using formatting `npm run lint`

# Testing

This application uses **Jest for unit testing** and **Supertest for integration tests**. Also, using **jest-extended** which is extend assertions that missing in jest's itself.

You can perform tests using `npm run test` also coverage reports `npm run coverage`

There are a few testing utils that i've used; 
MongoDB Memory Server - https://github.com/nodkz/mongodb-memory-server
Faker JS - https://github.com/marak/Faker.js/

Jest - https://jestjs.io/
Supertest - https://github.com/visionmedia/supertest
Jest Extended - https://github.com/jest-community/jest-extended

# LoadTest

Performed load test using 10 concurent user for 30 seconds, results are below.

Used tool: https://www.npmjs.com/package/loadtest

### Results:

```bash

❯ loadtest -t 30 -c 10  http://localhost:8000 -P '{ "startDate": "2010-01-01", "endDate": "2020-01-01", "minCount": 0, "maxCount": 1000 }' -T 'application/json'

[Sun Nov 29 2020 21:51:59 GMT+0300 (GMT+03:00)] INFO Requests: 0, requests per second: 0, mean latency: 0 ms
[Sun Nov 29 2020 21:52:04 GMT+0300 (GMT+03:00)] INFO Requests: 137, requests per second: 27, mean latency: 352 ms
[Sun Nov 29 2020 21:52:09 GMT+0300 (GMT+03:00)] INFO Requests: 289, requests per second: 30, mean latency: 330.4 ms
[Sun Nov 29 2020 21:52:14 GMT+0300 (GMT+03:00)] INFO Requests: 433, requests per second: 29, mean latency: 342.6 ms
[Sun Nov 29 2020 21:52:19 GMT+0300 (GMT+03:00)] INFO Requests: 586, requests per second: 31, mean latency: 329.2 ms
[Sun Nov 29 2020 21:52:24 GMT+0300 (GMT+03:00)] INFO Requests: 738, requests per second: 30, mean latency: 331.6 ms
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO 
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO Target URL:          http://localhost:8000
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO Max time (s):        30
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO Concurrency level:   10
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO Agent:               none
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO 
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO Completed requests:  891
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO Total errors:        0
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO Total time:          30.005861783 s
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO Requests per second: 30
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO Mean latency:        335.4 ms
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO 
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO Percentage of the requests served within a certain time
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO   50%      329 ms
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO   90%      357 ms
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO   95%      369 ms
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO   99%      572 ms
[Sun Nov 29 2020 21:52:29 GMT+0300 (GMT+03:00)] INFO  100%      689 ms (longest request)

```