const { Schema, model } = require('mongoose');

const RecordSchema = new Schema({
  key: {
    type: String,
    unique: true,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  counts: [{
    type: Number,
  }],
  value: {
    type: String,
  },
});

const Record = model('Record', RecordSchema);
module.exports = Record;
