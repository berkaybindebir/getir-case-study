/* eslint-disable no-await-in-loop */
const mongoose = require('mongoose');
const faker = require('faker');
const moment = require('moment');

const { MongoMemoryServer } = require('mongodb-memory-server');

const connectMongoose = require('../loaders/mongoose');
const recordService = require('./record.service');
const Record = require('./record.model');

const mongod = new MongoMemoryServer();

/**
 * Remove all the data for all db collections.
 */
async function clearDatabase() {
  const { collections } = mongoose.connection;

  // eslint-disable-next-line no-restricted-syntax
  for (const key in collections) {
    if (collections[key]) {
      const collection = collections[key];
      await collection.deleteMany();
    }
  }
}

async function createMockRecord() {
  const newRecord = new Record({
    key: 'ZuuKxluZ',
    counts: [173, 260, 65, 8],
    value: 'Test',
  });

  const record = newRecord.save();
  return record;
}

describe('Record Service', () => {
  let mockRecord;
  beforeAll(async () => {
    const uri = await mongod.getUri();
    await connectMongoose(uri);
  });

  beforeEach(async () => {
    mockRecord = await createMockRecord();
  });

  it('should be defined', () => {
    expect(recordService).toBeDefined();
  });

  it('should be defined', () => {
    expect(Record).toBeDefined();
  });

  test('should return array of records with 1 record', async () => {
    const startDate = moment(faker.date.past()).format('YYYY-MM-DD');
    const endDate = moment(faker.date.future()).format('YYYY-MM-DD');
    const minCount = 10;
    const maxCount = 1000;

    const spy = jest.spyOn(recordService, 'getRecords');
    await expect(recordService.getRecords({
      startDate, endDate, minCount, maxCount,
    }))
      .resolves.toBeArrayOfSize(1);

    expect(spy).toHaveBeenCalledWith({
      startDate, endDate, minCount, maxCount,
    });

    spy.mockRestore();
  });

  test('should satisfy mock record', async () => {
    const startDate = moment(faker.date.past()).format('YYYY-MM-DD');
    const endDate = moment(faker.date.future()).format('YYYY-MM-DD');
    const minCount = 10;
    const maxCount = 1000;
    // eslint-disable-next-line max-len
    const isSatisfiedMockRecord = (el) => el.totalCount === mockRecord.counts.reduce((a, b) => a + b, 0);

    const spy = jest.spyOn(recordService, 'getRecords');
    await expect(recordService.getRecords({
      startDate, endDate, minCount, maxCount,
    }))
      .resolves.toSatisfyAll(isSatisfiedMockRecord);

    expect(spy).toHaveBeenCalledWith({
      startDate, endDate, minCount, maxCount,
    });

    spy.mockRestore();
  });

  test('should return empty array because of date range', async () => {
    const startDate = moment(faker.date.future()).format('YYYY-MM-DD');
    const endDate = moment(faker.date.future()).format('YYYY-MM-DD');
    const minCount = 10;
    const maxCount = 1000;

    const spy = jest.spyOn(recordService, 'getRecords');
    await expect(recordService.getRecords({
      startDate, endDate, minCount, maxCount,
    }))
      .resolves.toBeArrayOfSize(0);

    expect(spy).toHaveBeenCalledWith({
      startDate, endDate, minCount, maxCount,
    });

    spy.mockRestore();
  });

  test('should return empty array because of count range', async () => {
    const startDate = moment(faker.date.past()).format('YYYY-MM-DD');
    const endDate = moment(faker.date.future()).format('YYYY-MM-DD');
    const minCount = 10;
    const maxCount = 100;

    const spy = jest.spyOn(recordService, 'getRecords');
    await expect(recordService.getRecords({
      startDate, endDate, minCount, maxCount,
    }))
      .resolves.toBeArrayOfSize(0);

    expect(spy).toHaveBeenCalledWith({
      startDate, endDate, minCount, maxCount,
    });

    spy.mockRestore();
  });

  afterEach(async () => {
    await clearDatabase();
  });

  /**
 * Drop database, close the connection and stop mongod.
 */
  afterAll(async () => {
    await mongoose.connection.dropDatabase();
    await mongoose.connection.close();
    await mongod.stop();
  });
});
