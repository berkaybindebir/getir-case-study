const moment = require('moment');
const Record = require('./record.model');
/**
 * @description Represents the business logic for the get records by a given query.
 * Would be better to create a service class for each service and inject required dependencies
 * but in this case, this will be sufficient
 * @param {*} {
 *   startDate, endDate, minCount, maxCount,
 * }
 * @return {Promise} Resolves array of records
 */
async function getRecords({
  startDate, endDate, minCount, maxCount,
}) {
  try {
    const startTo = moment(startDate, 'YYYY-MM-DD').toDate();
    const endTo = moment(endDate, 'YYYY-MM-DD').toDate();

    /*
      Records must be filtered by
      field of createdDate startDate and endDate
      field of counts minCount and maxCount after sum of each value in array

      AGGERATION STAGES
      First, eliminate the records older than startDate and newer than endDate
      eliminating them in the first stage will be more efficient because
      next stages are more expensive
      In the Second stage, we defined the fields that we want to
      In the Third stage, we eliminate the records totalCount is
      less than minCount and greater than maxCount
      In the final stage, we sort the records by totalCount and createdDate

      There are lot of different approaches available in MongoDB
      https://docs.mongodb.com/manual/core/aggregation-pipeline-optimization/
    */
    const records = await Record.aggregate([
      {
        $match: { createdAt: { $gt: startTo, $lt: endTo } },
      },
      {
        $project: {
          totalCount: { $sum: '$counts' },
          key: 1,
          createdAt: 1,
          _id: 0,
        },
      },
      {
        $match: { totalCount: { $gt: minCount, $lt: maxCount } },
      },
      {
        $sort: { totalCount: -1, createdAt: -1 },
      },
    ]).exec();

    return records;
  } catch (error) {
    console.error(error);
    throw new Error('Something went wrong while getting records');
  }
}

module.exports = { getRecords };
