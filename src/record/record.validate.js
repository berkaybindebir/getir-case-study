const moment = require('moment');
const { body, validationResult } = require('express-validator');

exports.validateRecords = [
  body('startDate')
    .not().isEmpty()
    .withMessage('must be not empty')
    .isDate()
    .withMessage('must be date')
    .custom(async (startDate, { req }) => {
      const { endDate } = req.body;
      const momentStartDate = moment(startDate, 'YYYY-MM-DD');
      const momentEndDate = moment(endDate, 'YYYY-MM-DD');
      const isSameOfAfter = momentStartDate.isSameOrBefore(momentEndDate);

      if (!isSameOfAfter) {
        throw new Error('must be before endDate');
      }
    }),
  body('endDate')
    .not().isEmpty()
    .withMessage('must be not empty')
    .isDate()
    .withMessage('must be date')
    .custom(async (endDate, { req }) => {
      const { startDate } = req.body;
      const momentStartDate = moment(startDate, 'YYYY-MM-DD');
      const momentEndDate = moment(endDate, 'YYYY-MM-DD');
      const isSameOfAfter = momentEndDate.isSameOrAfter(momentStartDate);

      if (!isSameOfAfter) {
        throw new Error('must be after startDate');
      }
    }),
  body('minCount')
    .not()
    .isEmpty()
    .withMessage('must be not empty')
    .isNumeric()
    .withMessage('must be number')
    .custom(async (minCount, { req }) => {
      const { maxCount } = req.body;

      if (minCount > maxCount) {
        throw new Error('must be less than or equal to maxCount');
      }
    }),
  body('maxCount')
    .not()
    .isEmpty()
    .withMessage('must be not empty')
    .isNumeric()
    .withMessage('must be number')
    .custom(async (maxCount, { req }) => {
      const { minCount } = req.body;

      if (maxCount < minCount) {
        throw new Error('must be greater than or equal to minCount');
      }
    }),
];

exports.validateMiddleware = (req, res, next) => {
  const errors = validationResult(req).formatWith(({ msg, param }) => `${param} ${msg}`);

  if (!errors.isEmpty()) {
    return res.status(422).json({
      code: 1,
      msg: 'Error',
      errors: errors.array(),
    });
  }

  return next();
};
