/* eslint-disable no-use-before-define */
/* eslint-disable no-await-in-loop */
const mongoose = require('mongoose');
const faker = require('faker');
const moment = require('moment');
const request = require('supertest');
const { MongoMemoryServer } = require('mongodb-memory-server');

const APIServer = require('../apps/api');

const mongod = new MongoMemoryServer();

const connectMongoose = require('../loaders/mongoose');
const recordService = require('./record.service');
const Record = require('./record.model');

describe('Record Endpoint', () => {
  let app;
  beforeAll(async () => {
    const uri = await mongod.getUri();
    await connectMongoose(uri);
    app = APIServer.getApplication();
  });

  beforeEach(async () => {
    await createMockRecord();
  });

  it('should be defined', () => {
    expect(recordService).toBeDefined();
  });

  it('should be defined', () => {
    expect(Record).toBeDefined();
  });

  it('should 422 with empty or missing fields', async () => {
    const res = await request(app)
      .post('/')
      .send({});

    expect(res.statusCode).toEqual(422);
    expect(res.body).toEqual({
      code: 1,
      msg: 'Error',
      errors: [
        'startDate must be not empty',
        'startDate must be date',
        'startDate must be before endDate',
        'endDate must be not empty',
        'endDate must be date',
        'endDate must be after startDate',
        'minCount must be not empty',
        'minCount must be number',
        'maxCount must be not empty',
        'maxCount must be number',
      ],
    });
  });

  it('should 200 with code 0 and success message', async () => {
    const startDate = moment(faker.date.past()).format('YYYY-MM-DD');
    const endDate = moment(faker.date.future()).format('YYYY-MM-DD');
    const minCount = 10;
    const maxCount = 1000;

    const res = await request(app)
      .post('/')
      .send({
        startDate, endDate, minCount, maxCount,
      });

    expect(res.statusCode).toEqual(200);
    expect(res.body).toContainEntries([['code', 0], ['msg', 'Success']]);
  });

  it('should 200 with array of records containing 1 item', async () => {
    const startDate = moment(faker.date.past()).format('YYYY-MM-DD');
    const endDate = moment(faker.date.future()).format('YYYY-MM-DD');
    const minCount = 10;
    const maxCount = 1000;

    const res = await request(app)
      .post('/')
      .send({
        startDate, endDate, minCount, maxCount,
      });

    expect(res.statusCode).toEqual(200);
    expect(res.body.records).toBeArrayOfSize(1);
  });

  it('should 200 with array of records containing 0 item', async () => {
    const startDate = moment(faker.date.past()).format('YYYY-MM-DD');
    const endDate = moment(faker.date.future()).format('YYYY-MM-DD');
    const minCount = 10;
    const maxCount = 20;

    const res = await request(app)
      .post('/')
      .send({
        startDate, endDate, minCount, maxCount,
      });

    expect(res.statusCode).toEqual(200);
    expect(res.body.records).toBeArrayOfSize(0);
  });

  afterEach(async () => {
    await clearDatabase();
  });

  /**
 * Drop database, close the connection and stop mongod.
 */
  afterAll(async () => {
    await mongoose.connection.dropDatabase();
    await mongoose.connection.close();
    await mongod.stop();
  });
});

// TEST UTILS
/**
 * Creates new mock record
 */
async function createMockRecord() {
  const newRecord = new Record({
    key: 'ZuuKxluZ',
    counts: [173, 260, 65, 8],
    value: 'Test',
  });

  const record = newRecord.save();
  return record;
}

/**
 * Remove all the data for all db collections.
 */
async function clearDatabase() {
  const { collections } = mongoose.connection;

  // eslint-disable-next-line no-restricted-syntax
  for (const key in collections) {
    if (collections[key]) {
      const collection = collections[key];
      await collection.deleteMany();
    }
  }
}
