const { getRecords } = require('./record.service');

exports.getRecords = async (req, res, next) => {
  try {
    const {
      startDate,
      endDate,
      minCount,
      maxCount,
    } = req.body;

    const records = await getRecords({
      startDate, endDate, minCount, maxCount,
    });

    res.status(200).json({
      code: 0,
      msg: 'Success',
      records,
    });
  } catch (error) {
    next(error);
  }
};
