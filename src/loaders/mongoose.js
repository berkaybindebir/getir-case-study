const mongoose = require('mongoose');
const chalk = require('chalk');

/**
 * @description Mongoose deprecation warnings
 * @link https://mongoosejs.com/docs/deprecations.html
*/
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);

/**
 *
 * @description returns of mongoose connection
 * @param {*} uri
 * @return {*} Mongoose connection
 */
function connect(uri) {
  mongoose.connection.on('connected', () => {
    console.log('%s MongoDB connection Success.', chalk.green('✓'));
  });

  mongoose.connection.on('error', (err) => {
    console.error(err);
    console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));

    throw err;
  });

  console.info(chalk.yellow('Waiting Mongo connection..'));
  return mongoose.connect(uri);
}

module.exports = (uri) => connect(uri);
