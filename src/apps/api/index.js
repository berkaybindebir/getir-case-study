const chalk = require('chalk');
const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const helmet = require('helmet');
const cors = require('cors');

const routes = require('./routes');

/**
 *
 * @description Creates a new Application
 * @class APIServer
 */
class APIServer {
  constructor() {
    this.app = express();
  }

  /**
   *
   * @description Prepare express server for use
   * @memberof APIServer
   */
  setup() {
    this.app.use(helmet());
    this.app.use(cors());
    this.app.use(compression());
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));

    // Register routes
    this.app.use('/', routes);

    // Basic error handling for server errors
    this.app.use((err, req, res, next) => {
      if (!err) next();

      console.error('Error happend: ', err);
      return res.status(500).json({ message: 'Something went wrong!' });
    });
  }

  /**
   *
   * @param {Number} port - the port to listen
   * @param {String} host - the host to listen
   * @memberof APIServer
   */
  listen(port, host) {
    if (!host) {
      throw new Error('host must be specified');
    }

    if (!port) {
      throw new Error('port must be specified');
    }

    this.app.listen(port, host, () => {
      try {
        console.log('%s API Server is running at http://%s:%d in %s mode', chalk.green('✓'), host, port, process.env.NODE_ENV);
        console.log('  Press CTRL-C to stop\n');
      } catch (error) {
        console.log(chalk.red(error.message));
        process.exit(1);
      }
    });
  }

  /**
   * Setup the application listen
   * @param {Number} port - the port to listen
   * @param {String} host - the host to listen
   * @memberof APIServer
   */
  start(port, host) {
    this.setup();
    this.listen(port, host);
  }

  /**
   * @description Set up the application and returns it (This will use in supertest for testing)
   * @return {Express} The application
   * @memberof APIServer
   */
  getApplication() {
    this.setup();
    return this.app;
  }
}

module.exports = new APIServer();
