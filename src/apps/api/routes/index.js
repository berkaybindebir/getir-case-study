const express = require('express');

const { validateRecords, validateMiddleware } = require('../../../record/record.validate');
const { getRecords } = require('../../../record/record.controller');

const router = express.Router();

router.route('/')
  .get((req, res) => res.status(200).json({ message: 'Getir Case Study' }))
  .post([validateRecords, validateMiddleware, getRecords]);

module.exports = router;
